Depository is backup system with deduplication.

Usage:
======

To use it you need to create configuration file for your backup.
Here we have our example `backup.yaml` configuration file:
```
storage: /tmp/depo
sources: /etc
excludes:
- mtab
- shadow
```

To create backup you need to run:
```
depository backup backup.yaml
```

To check the integrity of the storage you can run:

```
depository check backup.yaml
```

To cleanup old backups you can use:

```
depository cleanup backup.yaml
```
By default we keep up to 30 days old backups or at least one backup.


Requirements:
=============
```
pip install python-magic PyYAML
```

on Windows also:
```
pip install python-magic-bin
```

on CentOS:
```
sudo yum install python-pathlib python-pip
sudo pip install python-magic
```

on Ubuntu use Python 3 as with Python 2 there is some issue with encoding on some systems:
```
sudo apt-get install python3-pip python3-yaml
sudo pip3 install python-magic
```
