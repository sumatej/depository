#!/usr/bin/env python
#
# Copyright (c) Matej Sujan 2018. Licensed under the EUPL-1.2 or later.
#


import argparse
import bz2
import fnmatch
import gzip
import hashlib
import logging
import logging.handlers
import magic
import os
import pathlib
import shutil
import sqlite3
import stat
import sys
import time
import yaml


class Depository:
    def __init__(self, database=':memory:', debug=[]):
        self.database = database
        self.debug = debug
        self.text_file_compression = 'bz2'
        self.text_file_minimal_size_to_compress = 500
        self.chunk_size = 1024 * 1024 * 512  # 512 MB
        self.minimal_free_space_after_backup = 1024 * 1024 * 1024  # 1 GB
        self.days_to_keep = 30
        self.keep_at_least = 1
        self.__tables_structure = {
            'depo_receipt': """
                 id integer PRIMARY KEY AUTOINCREMENT,
                 book_id integer,
                 origin text,
                 description text,
                 start integer,
                 finish integer,
                 status integer,
                 stat_directories integer,
                 stat_symlinks integer,
                 stat_files integer,
                 stat_files_size integer,
                 stat_added_files integer,
                 stat_added_files_size integer,
                 stat_changed_files integer,
                 stat_changed_files_size integer,
                 stat_changed_mime_type integer,
                 stat_storage_size_increase integer,
                 stat_failed integer,
                 discard integer""",
            'depo_object': """
                 sha text PRIMARY KEY,
                 size integer,
                 magic text,
                 locker text""",
            'depo_item': """
                 id integer PRIMARY KEY,
                 receipt_id integer,
                 path text,
                 name text,
                 sha text,
                 type text,
                 mode integer,
                 uid integer,
                 gid integer,
                 ctime integer,
                 mtime integer,
                 UNIQUE (receipt_id, path, name)""",
        }
        self.config = None
        self.storage = None
        self.includes = []
        self.includes_with_dir = []
        self.excludes = []
        self.excludes_with_dir = []
        self.db_columns_names = None
        self.__db_table_col_list = {}
        self.__conn = None
        self.__c = None
        self.__logger = None
        self.__book_id = None
        self.__receipt_id = None
        self.__previous_receipt_id = None
        self.__storage_free_space = 0
        self.__configure_logger()

    def __configure_logger(self, logfile=None, console=True):
        """ Configure logger """
        if self.__logger is None:
            self.__logger = logging.getLogger('Depository')
            self.__logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            '[%(asctime)s] %(levelname)s: %(message)s')
        self.__logger.handlers = []
        # create console handler with a higher log level
        if console:
            ch = logging.StreamHandler()
            if logfile is None:
                ch.setLevel(logging.DEBUG)
            else:
                ch.setLevel(logging.INFO)
            ch.setFormatter(formatter)
            self.__logger.addHandler(ch)
        # create file handler
        if logfile is not None:
            fh = logging.handlers.RotatingFileHandler(
                logfile, backupCount=1, encoding="UTF-8")
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(formatter)
            fh.doRollover()
            self.__logger.addHandler(fh)

    def __open_file(self, filename, mode='rb', compresslevel=9):
        """ return file handler """
        if filename.endswith('.gz'):
            return gzip.GzipFile(filename, mode, compresslevel)
        elif filename.endswith('.bz2'):
            return bz2.open(filename, mode, compresslevel)
        else:
            return open(filename, mode)

    def log_debug(self, msg, *args, **kwargs):
        try:
            self.__logger.debug(msg, *args, **kwargs)
        except Exception:
            pass

    def log_info(self, msg, *args, **kwargs):
        try:
            self.__logger.info(msg, *args, **kwargs)
        except Exception:
            pass

    def log_warning(self, msg, *args, **kwargs):
        try:
            self.__logger.warning(msg, *args, **kwargs)
        except Exception:
            pass

    def log_error(self, msg, *args, **kwargs):
        try:
            self.__logger.error(msg, *args, **kwargs)
        except Exception:
            pass

    def get_file_info(self, filename):
        """ Return file information like its size, owner, etc.  """
        try:
            file_stat = os.stat(filename)
            file_info = {
                'size': file_stat.st_size,
                'mode': file_stat.st_mode,
                'uid': file_stat.st_uid,
                'gid': file_stat.st_gid,
                'ctime': int(file_stat.st_ctime * 1000),
                'mtime': int(file_stat.st_mtime * 1000),
            }
            if os.path.islink(filename):
                file_info['type'] = 'symlink'
                file_info['magic'] = 'symlink'
            elif stat.S_ISREG(file_info['mode']):
                file_info['type'] = 'file'
                file_info['magic'] = magic.from_file(filename, mime=True)
                if file_info['magic'].startswith('cannot open'):
                    file_info['magic'] = 'unknown'
            elif stat.S_ISDIR(file_info['mode']):
                file_info['type'] = 'directory'
                file_info['magic'] = 'directory'
            elif stat.S_ISFIFO(file_info['mode']):
                file_info['type'] = 'special-file'
                file_info['magic'] = 'FIFO'
            elif stat.S_ISSOCK(file_info['mode']):
                file_info['type'] = 'special-file'
                file_info['magic'] = 'socket'
            else:
                file_info['type'] = 'special-device'
                file_info['magic'] = 'special-device'
        except (OSError, IOError):
            file_info = None
        return file_info

    def get_file_checksum(self, filename):
        """ Return checksum of the file """
        hs = hashlib.sha1()
        try:
            with self.__open_file(filename, "rb") as fp:
                for chunk in iter(lambda: fp.read(self.chunk_size), b""):
                    hs.update(chunk)
            return hs.hexdigest()
        except IOError:
            return None

    def locker_path(self, locker, file_info=None, section='lockers'):
        """ Get the directory path for the locker """
        extension = '.raw'
        try:
            text_file_mime_types = [
                'text/', 'application/xml', 'model/x3d', 'image/svg+xml']
            for mime_type in text_file_mime_types:
                if file_info['magic'].startswith(mime_type):
                    if (int(file_info['size']) >=
                            self.text_file_minimal_size_to_compress):
                        extension = '.' + self.text_file_compression
                    break
        except (KeyError, ValueError):
            pass
        if section == 'lockers':
            path = os.path.join(self.storage, section, locker[:2])
        else:
            path = os.path.join(self.storage, section)
        if not os.path.exists(path):
            os.makedirs(path)
        path = os.path.join(path, locker + extension)
        return path

    def store_item(self, source_file, file_info, file_previous_info):
        """ Returns file sha and location """
        dummy = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        hs = hashlib.sha1()
        source = open(source_file, 'rb')
        chunk = source.read(self.chunk_size)
        hs.update(chunk)
        sha = hs.hexdigest()
        try:
            if int(file_info['size']) <= self.chunk_size:
                use_dummy_file = False
            else:
                raise IOError('[Errno 27]: ' + os.strerror(27))
        except IOError:
            use_dummy_file = True
        if use_dummy_file:
            destination_file = self.locker_path(
                dummy, file_info, section='desk')
        else:
            destination_file = self.locker_path(sha, file_info)
            locker_info = self.get_file_info(destination_file)
            if (locker_info is not None and
                    all(locker_info.get(par, 0) == file_info.get(par, 1)
                        for par in ['size', 'magic'])):
                source.close()
                self.log_debug('File {} ({})'.format(
                    source_file, self.bytes_to_human_readable(
                        file_info['size'])))
                return [sha, destination_file[len(self.storage) + 1:]]
        self.log_info('Storing file {} ({})'.format(
            source_file, self.bytes_to_human_readable(file_info['size'])))
        destination = self.__open_file(destination_file, 'wb')
        destination.write(chunk)
        for chunk in iter(lambda: source.read(self.chunk_size), b""):
            hs.update(chunk)
            destination.write(chunk)
        source.close()
        destination.close()
        sha = hs.hexdigest()
        if destination_file.find(dummy) != -1:
            old_destination_file = destination_file
            destination_file = self.locker_path(sha, file_info)
            if os.path.exists(destination_file):
                os.remove(old_destination_file)
            else:
                os.rename(old_destination_file, destination_file)
        if 'sha' in file_previous_info:
            self.origin_stat['stat_changed_files'] += 1
            self.origin_stat['stat_changed_files_size'] += int(
                file_info['size'])
        else:
            self.origin_stat['stat_added_files'] += 1
            self.origin_stat['stat_added_files_size'] += int(file_info['size'])
        locker_size = os.stat(destination_file).st_size
        self.origin_stat['stat_storage_size_increase'] += locker_size
        self.__storage_free_space -= locker_size
        return [sha, destination_file[len(self.storage) + 1:]]

    def db_sql_cmd(self, sql_cmd, commit=False):
        """ Run SQL command """
        if self.__conn is None:
            self.__conn = sqlite3.connect(self.database)
        if self.__c is None:
            self.__c = self.__conn.cursor()
        if self.debug is not None and 'sql' in self.debug:
            self.log_debug(sql_cmd)
        try:
            rsp = self.__c.execute(sql_cmd)
        except sqlite3.OperationalError:
            self.log_error(sql_cmd)
            raise
        if self.__c.description is not None:
            self.db_columns_names = [
                tuple[0] for tuple in self.__c.description]
        else:
            self.db_columns_names = None
        if commit:
            self.__conn.commit()
        return rsp

    def db_get_column_list(self, table):
        """ get the list of table columns """
        self.db_sql_cmd('SELECT * FROM {} LIMIT 0'.format(table))
        return self.db_columns_names[:]

    def db_init(self, database=None):
        """ create database and tables if empty """
        if database is not None:
            self.database = database
        for table, structure in self.__tables_structure.items():
            self.db_sql_cmd(
                "CREATE TABLE IF NOT EXISTS {} ({})".format(table, structure))
        self.__conn.commit()
        for table in self.__tables_structure.keys():
            self.__db_table_col_list[table] = self.db_get_column_list(table)

    def db_update(self, table, columns, where, commit=False):
        """ execute sql update """
        params = []
        for par, val in columns.items():
            if par in self.__db_table_col_list[table]:
                params.append("{} = '{}'".format(
                    par, str(val).replace("'", "''")))
        where_list = []
        for par, val in where.items():
            where_list.append("{} = '{}'".format(
                par, str(val).replace("'", "''")))
        self.db_sql_cmd("UPDATE {} SET {} WHERE {}".format(
            table, ', '.join(params), ', '.join(where_list)), commit)
        return self.__c.lastrowid

    def db_insert(self, table, columns, ignore_duplicate=False):
        """ insert record into db """
        parameters = []
        values = []
        for par, val in columns.items():
            if par not in self.__db_table_col_list[table]:
                continue
            parameters.append(str(par))
            values.append(str(val).replace("'", "''"))
        sql_cmd = "INSERT"
        if ignore_duplicate:
            sql_cmd += " OR IGNORE"
        self.db_sql_cmd("{} INTO {} ({}) VALUES('{}')".format(
            sql_cmd, table, ', '.join(parameters), "', '".join(values)))
        return self.__c.lastrowid

    def db_file_add(self, file_info):
        """ insert information about a file into db """
        self.db_insert('depo_item', file_info)
        if file_info['type'] == 'file':
            self.db_insert('depo_object', file_info, ignore_duplicate=True)

    def add_item(self, base_path, relative_path, base_name):
        """ Add item into storage """
        full_path = os.path.join(base_path, relative_path, base_name)
        file_info = self.get_file_info(full_path)
        if file_info is None:
            if os.path.islink(full_path) and not os.path.exists(full_path):
                file_info = {'type': 'symlink'}
            else:
                self.origin_stat['stat_failed'] += 1
                self.log_warning("Failed to store " + full_path)
                return False
        row = self.db_sql_cmd('SELECT * FROM depo_item WHERE receipt_id = "{}" and path = "{}" and name = "{}"'.format(
            self.__previous_receipt_id, relative_path, base_name)).fetchone()
        try:
            file_previous_info = dict(
                zip([i for i in self.db_columns_names], row))
        except (KeyError, TypeError):
            file_previous_info = {}
        file_info.update({
            'path': relative_path,
            'name': base_name,
            'receipt_id': self.__receipt_id,
        })
        if file_info['type'] == 'directory':
            file_info['sha'] = 'directory'
            self.origin_stat['stat_directories'] += 1
            self.log_debug('Directory ' + full_path)
        elif file_info['type'] == 'symlink':
            file_info['sha'] = 'symlink:' + os.readlink(full_path)
            self.origin_stat['stat_symlinks'] += 1
            self.log_debug('Symlink {} ({})'.format(
                full_path, file_info['sha'][8:]))
        elif file_info['type'] == 'file':
            self.origin_stat['stat_files'] += 1
            self.origin_stat['stat_files_size'] += int(file_info['size'])
            """ Compare file info from the latest backup """
            try:
                query = 'SELECT * FROM depo_object WHERE sha = "{}"'.format(
                    file_previous_info['sha'])
                row = self.db_sql_cmd(query).fetchone()
                file_previous_info.update(
                    dict(zip([i for i in self.db_columns_names], row)))
                file_info['sha'] = file_previous_info['sha']
                need_to_store = False
                for par in ['size', 'magic', 'mode', 'uid', 'gid', 'ctime',
                            'mtime']:
                    if file_info[par] != file_previous_info[par]:
                        need_to_store = True
                        break
                if not need_to_store and not os.path.exists(os.path.join(
                        self.storage, file_previous_info['locker'])):
                    need_to_store = True
            except (KeyError, TypeError):
                need_to_store = True
            """ If the file has changed, store it """
            if need_to_store:
                used_space_after = (file_info['size'] +
                                    self.minimal_free_space_after_backup)
                if used_space_after > self.__storage_free_space:
                    if used_space_after > self.get_free_space():
                        self.log_error("Not enough free disk space ({})".format(
                            self.bytes_to_human_readable(
                                self.__storage_free_space)))
                        raise IOError('[Errno 28]: ' + os.strerror(28))
                file_info['sha'], file_info['locker'] = self.store_item(
                    full_path, file_info, file_previous_info)
            else:
                self.log_debug('File {} ({})'.format(
                    full_path, self.bytes_to_human_readable(
                        file_info['size'])))
        else:
            file_info['sha'] = file_info['type']
            self.origin_stat['stat_special_files'] += 1
            self.log_debug('{} {}'.format(
                file_info['type'].capitalize(), full_path))
        for par in ['magic', 'type']:
            if (par in file_previous_info and
                    file_info.get(par, '') != file_previous_info[par]):
                self.origin_stat['stat_changed_mime_type'] += 1
                break
        self.db_file_add(file_info)
        return True

    def bytes_to_human_readable(self, count):
        """ Show bytes in human readable form (bytes/kB/MB/GB/...) """
        for unit in ['B', 'kB', 'MB', 'GB', 'TB', 'PB']:
            if count > 1121:
                count /= 1024.0
            else:
                break
        value = round(count, 2)
        if value == int(value):
            value = int(value)
        return str(value) + ' ' + unit

    def get_free_space(self, path=None):
        """ return the free space """
        if path is None:
            path = self.storage
        try:
            free_space = shutil.disk_usage(path).free
        except AttributeError:
            statvfs = os.statvfs(path)
            free_space = statvfs.f_frsize * statvfs.f_bavail
        if path == self.storage:
            self.__storage_free_space = free_space
        return free_space

    def storage_init(self, path):
        """ Initialize storage """
        if not os.path.exists(path):
            for sub_dir in ['desk', 'lockers']:
                os.makedirs(os.path.join(self.storage, sub_dir))

    def configure(self, config_file):
        """ Read configuration file """
        try:
            self.config = yaml.safe_load(open(config_file))
            self.storage = os.path.abspath(self.config['storage'])
            self.database = os.path.join(self.storage, 'storage.db')
        except (TypeError, KeyError) as err:
            if str(err).startswith("'NoneType'"):
                self.log_error('Configuration file is empty')
            else:
                self.log_error(
                    'Missing mandatory parameter {} in the configuration file'.format(err))
            sys.exit(1)
        except (yaml.scanner.ScannerError, IOError):
            self.log_error(
                'Failed to load config file "{}"'.format(config_file))
            sys.exit(1)
        if ('compression' in self.config.get('text_file', {}) and
                self.config['text_file']['compression'] in ['gz', 'bz2',
                                                            'raw']):
            self.text_file_compression = self.config['text_file']['compression']
        if 'minimal_size_to_compress' in self.config.get('text_file', {}):
            try:
                self.text_file_minimal_size_to_compress = int(
                    self.config['text_file']['minimal_size_to_compress'])
            except ValueError:
                pass
        if 'minimal_free_space_after_backup' in self.config:
            try:
                self.minimal_free_space_after_backup = int(
                    self.config['minimal_free_space_after_backup'])
            except ValueError:
                pass

    def path_included(self, path):
        """ Check if path is included """
        if len(self.includes_with_dir) + len(self.includes) == 0:
            return True
        for pattern in self.includes_with_dir:
            if fnmatch.fnmatch(path, pattern):
                return True
        for p in path.split(os.path.sep):
            for pattern in self.includes:
                if fnmatch.fnmatch(p, pattern):
                    return True
        return False

    def path_excluded(self, path):
        """ Check if path is excluded """
        for pattern in self.excludes_with_dir:
            if fnmatch.fnmatch(path, pattern):
                return True
        for p in path.split(os.path.sep):
            for pattern in self.excludes:
                if fnmatch.fnmatch(p, pattern):
                    return True
        return False

    def log_stat(self, backup_stat):
        """ log stat of backup """
        human_stat = {}
        for par, val in backup_stat.items():
            if par.find('_size') != -1:
                human_stat[par] = self.bytes_to_human_readable(backup_stat[par])
            else:
                human_stat[par] = backup_stat[par]
        self.log_info(
            '''Storage size increase {stat_storage_size_increase}, Changed mime type: {stat_changed_mime_type}'''.format(**human_stat))
        self.log_info(
            '''New files: {stat_added_files} ({stat_added_files_size}), Changed files: {stat_changed_files} ({stat_changed_files_size})'''.format(**human_stat))
        if int(human_stat['stat_special_files']) > 0:
            self.log_info(
                '''Number of special files like pipe (FIFO), socket or character/block device: {stat_special_files}'''.format(**human_stat))
        self.log_info(
            '''Total: {stat_files} files ({stat_files_size}), {stat_symlinks} symlinks and {stat_directories} directories'''.format(**human_stat))
        if int(human_stat['stat_failed']) > 0:
            self.log_warning(
                '''Failed to store {stat_failed} files'''.format(**human_stat))

    def run_backup(self, config_file):
        """ run command backup """
        if self.config is None:
            self.configure(config_file)
        try:
            if not isinstance(self.config['sources'], list):
                self.config['sources'] = [self.config['sources']]
            sources = self.config['sources']
        except KeyError:
            if 'source' in self.config:
                self.log_info(
                    "You want to change 'source' to 'sources' in your config file.")
            self.log_error("No sources specified in the config, terminating")
            return False
        if 'excludes' in self.config:
            if not isinstance(self.config['excludes'], list):
                self.config['excludes'] = [self.config['excludes']]
            for pattern in self.config['excludes']:
                if pattern.find('/') != -1:
                    self.excludes_with_dir.append(pattern)
                else:
                    self.excludes.append(pattern)
        if 'includes' in self.config:
            if not isinstance(self.config['includes'], list):
                self.config['includes'] = [self.config['includes']]
            for pattern in self.config['includes']:
                if pattern.find('/') != -1:
                    self.includes_with_dir.append(pattern)
                else:
                    self.includes.append(pattern)
        if os.path.exists(self.storage):
            shutil.copy2(self.database, self.database + '.1')
        else:
            self.storage_init(self.storage)
        self.__configure_logger(os.path.join(self.storage, 'depository.log'))
        self.log_info('Storage {} (free space {})'.format(
            self.storage, self.bytes_to_human_readable(self.get_free_space())))
        self.db_init()
        self.backup_stat = {
            'stat_directories': 0,
            'stat_symlinks': 0,
            'stat_files': 0,
            'stat_files_size': 0,
            'stat_special_files': 0,
            'stat_added_files': 0,
            'stat_added_files_size': 0,
            'stat_changed_files': 0,
            'stat_changed_files_size': 0,
            'stat_changed_mime_type': 0,
            'stat_storage_size_increase': 0,
            'stat_failed': 0,
        }
        try:
            for origin in sources:
                self.origin_stat = {}
                for par in self.backup_stat.keys():
                    self.origin_stat[par] = 0
                origin = os.path.abspath(origin)
                self.log_info('Source directory ' + origin)
                try:
                    self.__previous_receipt_id = self.db_sql_cmd(
                        'SELECT id FROM depo_receipt WHERE origin = "{}" ORDER BY start DESC LIMIT 1'.format(origin)).fetchone()[0]
                except TypeError:
                    self.__previous_receipt_id = None
                self.__receipt_id = self.db_insert(
                    'depo_receipt', {
                        'origin': origin,
                        'description': 'Backup of ' + origin,
                        'start': int(time.time() * 1000),
                    })
                if self.__book_id is None:
                    self.__book_id = self.__receipt_id
                for (path, directories, files) in os.walk(origin):
                    try:
                        relative_path = os.path.sep.join(pathlib.Path(
                            path).parts[len(pathlib.Path(origin).parts):])
                    except Exception:
                        relative_path = path[len(origin) + 1:]
                    for base_name in directories[:]:
                        if self.path_excluded(
                                os.path.join(relative_path, base_name)):
                            directories.remove(base_name)
                    for base_name in directories:
                        self.add_item(origin, relative_path, base_name)
                    for base_name in files:
                        if not self.path_included(
                                os.path.join(relative_path, base_name)):
                            continue
                        if self.path_excluded(
                                os.path.join(relative_path, base_name)):
                            continue
                        if not self.add_item(origin, relative_path, base_name):
                            break
                self.origin_stat['book_id'] = self.__book_id
                self.origin_stat['finish'] = int(time.time() * 1000)
                self.db_update('depo_receipt', self.origin_stat,
                               {'id': self.__receipt_id}, commit=True)
                for par in self.backup_stat.keys():
                    self.backup_stat[par] += self.origin_stat[par]
        except IOError:
            pass
        self.log_stat(self.backup_stat)
        return int(self.backup_stat['stat_failed']) == 0

    def run_check(self, config_file):
        """ run command check """
        if self.config is None:
            self.configure(config_file)
        check_passed = True
        if not os.path.exists(self.storage):
            self.log_error('There is no storage in "{}", you need to run backup first'.format(
                self.storage))
            return False
        count_total = count_failed = 0
        query = 'SELECT sha, locker FROM depo_object'
        for sha, filename in self.db_sql_cmd(query).fetchall():
            count_total += 1
            if sha == self.get_file_checksum(
                    os.path.join(self.storage, filename)):
                self.log_debug('Locker "{}" verified'.format(filename))
            else:
                self.log_warning(
                    'Verification of locker "{}" failed'.format(filename))
                count_failed += 1
                check_passed = False
        self.log_info('Checked {} lockers, {} failed'.format(count_total,
                                                             count_failed))
        return check_passed

    def run_migrate(self, config_file):
        """ run command migrate """
        if self.config is None:
            self.configure(config_file)
        migration_passed = True
        if os.path.exists(self.storage):
            shutil.copy2(self.database, self.database + '.1')
        else:
            self.storage_init(self.storage)
        tables_to_migrate = ['depo_receipt']
        for table in tables_to_migrate:
            self.db_sql_cmd('ALTER TABLE {0} RENAME TO {0}_old'.format(table))
        self.db_init()
        for table in tables_to_migrate:
            columns = []
            old_table_col_list = self.db_get_column_list(table + '_old')
            for col in self.__db_table_col_list[table]:
                if col in old_table_col_list:
                    columns.append(col)
                else:
                    columns.append('null')
            self.db_sql_cmd('INSERT INTO {0} SELECT {1} FROM {0}_old'.format(
                table, ', '.join(columns)))
            self.db_sql_cmd('DROP TABLE {0}_old'.format(table), commit=True)
        return migration_passed

    def __get_files(self, args, files):
        """Fetches all files and directories from the storage.db"""
        sql_cmd = f"select o.locker, i.name, i.sha, i.path from depo_item i left join depo_object o on i.sha = o.sha left join depo_receipt r on r.id = i.receipt_id WHERE "
        for file in files:
            if file[-1] == '/':
                file = file[:-1]
            if os.path.isabs(file):
                sql_cmd += f"r.origin || '/' || i.path || IIF(i.path = '', '', '/') || i.name LIKE '{file}' OR "
            else:
                sql_cmd += f"i.name LIKE '{file}' AND i.receipt_id = {args.backup_id} OR "
        sql_cmd = sql_cmd[:-3] + "ORDER BY o.locker NULLS LAST, i.id DESC"
        return self.db_sql_cmd(sql_cmd).fetchall()

    def __get_children(self, name):
        """fetches all children of the specified path"""
        sql_cmd = f"select o.locker, i.name, i.sha, i.path from depo_item i left join depo_object o on i.sha = o.sha WHERE i.path = '{name}'"
        return self.db_sql_cmd(sql_cmd).fetchall()

    def list_backups(self, args):
        """Lists all backups"""
        # print(args)
        try:
            if args.backup_id != -1:
                self.__list_backup_files(args)
            else:
                sql_cmd = f"SELECT id, stat_files, origin FROM depo_receipt"
                otp = self.db_sql_cmd(sql_cmd).fetchall()
                print("-" * 37 + "\nID      |Files count |Origin path\n" + "-" * 37)  # 37
                if otp == []:
                    print("There were no backups found.")
                for row in otp:
                    id = row[0]
                    no_files = row[1]
                    origin = row[2]
                    print(f"{id}" + " " * (8 - len(str(id))) + "|" + f"{no_files}" +
                        " " * (12 - len(str(no_files))) + "|" + f"{origin}")
        except Exception as e:
            self.log_error(f"An error occured.\n{e}")
            return False
        return True

    def __list_backup_files(self, args):
        """lists all backups containing a file"""
        try:
            backup_id = int(args.backup_id[0])
            sql_cmd = f"SELECT i.id, i.receipt_id, i.name, i.path, r.origin, i.sha from depo_item i left join depo_object o on i.sha = o.sha left join depo_receipt r on r.id = i.receipt_id WHERE r.id = '{backup_id}' ORDER BY i.id ASC"
            otp = self.db_sql_cmd(sql_cmd).fetchall()
            print("-" * 37 + "\nID      |Type   |File path\n" + "-" * 37)  # 37
            if otp == []:
                print("There were no backups found.")
            for row in otp:
                id = row[0]
                file_path = os.path.join(row[4], row[3], row[2])
                type = "dir " if row[5] == "directory" else "file"
                print(f"{id}" + " " * (8 - len(str(id))) + "|" + f"{type}" + " " * (7 - 4) + "|" + f"{file_path}")
        except Exception as e:
            print(f"There was an error.\n{e}")
            return False
        return True

    def __handle_parent_path(self, args, path, name, flag: bool):
        """generates child directories"""
        if not flag:
            dir_path = os.path.join(args.output_file[0], path, name)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path, exist_ok=True)
        if flag:
            parent_path = os.path.join(args.output_file[0], path.strip('/'))
            if not os.path.exists(parent_path):
                os.makedirs(parent_path)

    def __get_latest_backup(self):
        """"""
        sql_cmd = "select count(*) from depo_receipt"
        otp = self.db_sql_cmd(sql_cmd).fetchall()
        return otp


    def run_restore(self, args):
        if self.config is None:
            self.configure(args.config)
        if os.path.exists(self.storage):
            shutil.copy2(self.database, self.database + '.1')
        else:
            self.storage_init(self.storage)
        if args.file_path == [] or args.file_path == None:
            self.log_error(
                'You are missing a restore_file.')
            return False
        if args.output_file == None:
            self.log_error("You are missing an output file.")
        if args.backup_id == -1:
            try:
                args.backup_id = self.__get_latest_backup()[0][0]
                print(args.backup_id)
            except sqlite3.OperationalError:
                self.log_error(
                'There is a problem with the database, the tables are possibly corrupted or dont exist.')
        otp = self.__get_files(args, args.file_path)
        if otp == []:
            print("no file was found.")
        else:
            try:
                storage_dir = self.config['storage']
                while True:
                    if otp == []:
                        break
                    (locker, name, sha, path) = otp.pop()
                    if sha == "directory":
                        self.__handle_parent_path(
                            args, path.strip('/'), name, False)
                        inner_otp = self.__get_children(
                            os.path.join(path, name))
                        if inner_otp != []:
                            for query in inner_otp:
                                otp.append(query)
                    else:  # It's a file
                        copy_path = os.path.join(
                            args.output_file[0], path.strip('/'), name)
                        self.__handle_parent_path(
                            args, path, name, True)
                        with open(copy_path, 'wb') as f:
                            full_path = os.path.join(storage_dir, locker)
                            f.write(self.__open_file(full_path).read())
                        print(f"{copy_path} was restored.")
            except Exception as e:
                self.log_error(
                    f"{e}\nThere was a problem opening your file.")
                raise
        return True

    def run_cleanup(self, config_file):
        """ run command cleanup """
        if self.config is None:
            self.configure(config_file)
        if os.path.exists(self.storage):
            shutil.copy2(self.database, self.database + '.1')
        else:
            self.storage_init(self.storage)
        days_to_keep = self.config.get('days_to_keep', self.days_to_keep)
        keep_at_least = self.config.get('keep_at_least', self.keep_at_least)
        print("Will remove backups older than {0} days.".format(days_to_keep))
        oldest_backup_to_keep = (int(time.time() * 1000) -
                                    (int(days_to_keep) * 86400000))
        backups_to_keep = 0
        backups_to_remove = []
        query = 'SELECT id, start FROM depo_receipt ORDER BY start DESC'
        for backup_id, backup_start in self.db_sql_cmd(query).fetchall():
            if backups_to_keep < keep_at_least:
                backups_to_keep += 1
            elif backup_start < oldest_backup_to_keep:
                backups_to_remove.append(str(backup_id))
        print(" - removing receipts")
        self.db_sql_cmd('DELETE FROM depo_receipt WHERE id IN ({0})'.format(
            ','.join(backups_to_remove)), commit=True)
        print(" - removing items")
        self.db_sql_cmd(
            'DELETE FROM depo_item WHERE id IN (SELECT depo_item.id FROM depo_item LEFT JOIN depo_receipt ON depo_item.receipt_id = depo_receipt.id WHERE start is Null)', commit=True)
        print(" - removing objects")
        objects_to_remove = []
        for object_sha, object_locker in self.db_sql_cmd('SELECT depo_object.sha, depo_object.locker FROM depo_object LEFT JOIN depo_item ON depo_object.sha = depo_item.sha WHERE depo_item.sha is Null').fetchall():
            if object_locker.find('/') == -1:
                print('error: Locker path for object {0} is corrupted ({1})'.format(
                    object_sha, object_locker))
                continue
            locker_path = os.path.join(self.storage, object_locker)
            try:
                if os.path.exists(locker_path):
                    os.unlink(locker_path)
                objects_to_remove.append(str(object_sha))
            except Exception:
                print('error: Failed to remove {} from the storage'.format(
                    locker_path))
                raise
        if len(objects_to_remove) > 0:
            query = 'DELETE FROM depo_object WHERE sha IN ("{}")'.format(
                '","'.join(objects_to_remove))
            self.db_sql_cmd(query, commit=True)
        """ show some statistics """
        query = 'SELECT COUNT(id) FROM depo_receipt'
        backups_count = self.db_sql_cmd(query).fetchone()[0]
        print("Storage now keeps {0} backup(s), we removed {1} backup(s).".format(
            backups_count, len(backups_to_remove)))
        return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('command', help='command to run')
    parser.add_argument('config', help='config file to use')
    restore_parser = argparse.ArgumentParser(parents=[parser])
    restore_parser.add_argument('-f', '--file-path', action='store', nargs='*',
                                help='name of the file to restore (regex allowed)', required=False)
    restore_parser.add_argument('-b', '--backup-id', action='store', nargs=1,
                                help='id of the backup that contains the file', required=False, default=-1)
    restore_parser.add_argument(
        '-o', '--output-file', action='store', nargs=1, help='output file path', required=False)
    args = restore_parser.parse_args()
    depo = Depository(debug=[])
    depo.configure(args.config)
    rc = 0
    if args.command == 'backup':
        if not depo.run_backup(args.config):
            rc = 2
    elif args.command == 'check':
        if not depo.run_check(args.config):
            rc = 2
    elif args.command == 'migrate':
        if not depo.run_migrate(args.config):
            rc = 2
    elif args.command == 'cleanup':
        if not depo.run_cleanup(args.config):
            rc = 2
    elif args.command == 'restore':
        if not depo.run_restore(args):
            rc = 2
    elif args.command == 'list':
        if not depo.list_backups(args):
            rc = 2
    else:
        depo.log_error('Unknown command "{}"'.format(args.command))
        rc = 1
    del depo
    sys.exit(rc)
